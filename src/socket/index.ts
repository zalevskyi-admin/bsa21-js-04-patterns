import { Server } from 'socket.io';
import { GameRoom } from './gameRoom.js';

const users = new Map();
const room = 'room';

export default (io: Server) => {
  const defaultRoom = new GameRoom(io, 'room');

  io.on('connection', socket => {
    const username = socket.handshake.query['username'];
    if (typeof username === 'string' && username.length > 0) {
      if (users.has(username)) {
        socket.disconnect();
      } else {
        users.set(username, socket.id);
        defaultRoom.add(username, socket);
        socket.join(room);
      }

      socket.on('disconnect', () => {
        users.delete(username);
        defaultRoom.delete(socket.id);
      });

      socket.on('READY', ready => {
        defaultRoom.setReady(socket.id, ready);
      });

      socket.on('TYPE', key => {
        defaultRoom.checkType(socket.id, key);
      });
    }
  });
};
