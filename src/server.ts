'use strict';
import express from 'express';
import http from 'http';
import routes from './routes/index.js';
import { STATIC_PATH, PORT } from './config.js';
import { Server } from 'socket.io';
import socketHandler from './socket/index.js';

const app = express();
const httpServer = new http.Server(app);
const io = new Server(httpServer);

app.use(express.static(STATIC_PATH));
routes(app);

socketHandler(io);

httpServer.listen(PORT, () => {
  console.log(`Server is listening on port ${PORT}`);
});

export { app, httpServer };
