import { Router } from 'express';
import path from 'path';
import { HTML_FILES_PATH } from '../config.js';
import { texts } from '../data.js';

const router = Router();

router.get('/', (_req, res) => {
  const page = path.join(HTML_FILES_PATH, 'game.html');
  res.sendFile(page);
});

router.get('/text/:id', (req, res) => {
  const textId = Number(req.params.id);
  if (Number.isNaN(textId) === false) {
    if (texts[textId]) {
      res.status(200).send({ data: texts[textId] });
      return;
    }
  }
  res.status(404).send({ error: 'There is no such text' });
});

export default router;
