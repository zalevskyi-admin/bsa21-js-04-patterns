export class UserIndicator {
  readonly name: string;
  readonly element: HTMLDivElement;
  readonly progressBar: ProgressBar;
  readonly status: StatusInidcator;
  constructor(name: string, ready: boolean, me: boolean) {
    this.name = name;
    this.element = document.createElement('div');
    this.progressBar = new ProgressBar(name);
    this.status = new StatusInidcator(ready);
    let nameDisplay = document.createElement('div');
    nameDisplay.classList.add('user-name');
    nameDisplay.innerText = me ? `${name} (You)` : name;
    this.element.classList.add('user-indicator');
    this.element.appendChild(this.status.element);
    this.element.appendChild(nameDisplay);
    this.element.appendChild(this.progressBar.element);
  }
}

export class StatusInidcator {
  readonly element: HTMLDivElement;
  constructor(ready: boolean) {
    this.element = document.createElement('div');
    this.element.classList.add('status-indicator');
    if (ready) {
      this.setReady();
    } else {
      this.setNotReady();
    }
  }
  setReady() {
    this.element.classList.add('status-ready');
    this.element.classList.remove('status-notready');
  }
  setNotReady() {
    this.element.classList.remove('status-ready');
    this.element.classList.add('status-notready');
  }
}

export class ProgressBar {
  readonly name: string;
  readonly element: HTMLDivElement;
  protected progressBar: HTMLDivElement;
  constructor(name: string) {
    this.name = name;
    this.element = document.createElement('div');
    this.progressBar = document.createElement('div');
    this.element.classList.add('user-progress-wrapper');
    this.progressBar.classList.add(this.name, 'user-progress', 'inprogress');
    this.element.appendChild(this.progressBar);
  }
  setProgress(fraction: number) {
    if (fraction >= 0) {
      if (fraction < 1) {
        this.progressBar.style.width = `${fraction * 100}%`;
        this.progressBar.classList.remove('finished');
        this.progressBar.classList.add('inprogress');
      } else {
        this.progressBar.style.width = `${100}%`;
        this.progressBar.classList.add('finished');
        this.progressBar.classList.remove('inprogress');
      }
    }
  }
}

export class PlaygroundText {
  readonly element: HTMLDivElement;
  protected doneCount: number = 0;
  protected text: string = '';
  protected textDone: HTMLSpanElement;
  protected textTodo: HTMLSpanElement;
  protected textLeft: HTMLSpanElement;
  constructor() {
    this.element = document.createElement('div');
    this.textDone = document.createElement('span');
    this.textTodo = document.createElement('span');
    this.textLeft = document.createElement('span');
    this.element.appendChild(this.textDone);
    this.element.appendChild(this.textTodo);
    this.element.appendChild(this.textLeft);
    this.textDone.classList.add('text-done');
    this.textTodo.classList.add('text-todo');
  }
  setText(text: string) {
    this.text = text;
    this.setDone(0);
  }
  getDoneCount() {
    return this.doneCount;
  }
  setDone(doneCount: number) {
    this.doneCount = doneCount;
    this.textDone.innerText = this.text.slice(0, doneCount);
    this.textTodo.innerText = this.text.slice(doneCount, doneCount + 1);
    this.textLeft.innerText = this.text.slice(doneCount + 1);
  }
  showMistake() {
    this.textTodo.classList.add('text-mistake');
    setTimeout(() => {
      this.textTodo.classList.remove('text-mistake');
    }, 500);
  }
}
